1 Peter 3:21  -  The like figure whereunto even baptism doth also now save us (not the putting away of the filth of the flesh, but the answer of a good conscience toward God,) by the resurrection of Jesus Christ:

1 Timothy 2:4  -  Who will have all men to be saved, and to come unto the knowledge of the truth.

1 Timothy 2:5  -  For there is one God, and one mediator between God and men, the man Christ Jesus;

Acts 4:12  -  Neither is there salvation in any other: for there is none other name under heaven given among men, whereby we must be saved.

Acts 20:21  -  Testifying both to the Jews, and also to the Greeks, repentance toward God, and faith toward our Lord Jesus Christ.

Romans 10:13  -  For whosoever shall call upon the name of the Lord shall be saved.

Romans 11:6  -  And if by grace, then is it no more of works: otherwise grace is no more grace. But if it be of works, then it is no more grace: otherwise work is no more work.

2 Corinthians 5:21  -  For he hath made him to be sin for us, who knew no sin; that we might be made the righteousness of God in him.

Hebrews 9:22  -  And almost all things are by the law purged with blood; and without shedding of blood is no remission.

Romans 6:23  -  For the wages of sin is death; but the gift of God is eternal life through Jesus Christ our Lord.

2 Corinthians 4:4  -  In whom the god of this world hath blinded the minds of them which believe not, lest the light of the glorious gospel of Christ, who is the image of God, should shine unto them.

Romans 3:23  -  For all have sinned, and come short of the glory of God;

Romans 5:12  -  Wherefore, as by one man sin entered into the world, and death by sin; and so death passed upon all men, for that all have sinned:

1 Corinthians 15:3  -  For I delivered unto you first of all that which I also received, how that Christ died for our sins according to the scriptures; 

1 Corinthians 15:4  -  And that he was buried, and that he rose again the third day according to the scriptures: 

1 Thessalonians 5:9  -  For God hath not appointed us to wrath, but to obtain salvation by our Lord Jesus Christ,

2 Timothy 4:3  -  For the time will come when they will not endure sound doctrine; but after their own lusts shall they heap to themselves teachers, having itching ears;

Matthew 1:23  -  Behold, a virgin shall be with child, and shall bring forth a son, and they shall call his name Emmanuel, which being interpreted is, God with us.

Isaiah 7:14  -  Therefore the Lord himself shall give you a sign; Behold, a virgin shall conceive, and bear a son, and shall call his name Immanuel.

Isaiah 53:7  -  He was oppressed, and he was afflicted, yet he opened not his mouth: he is brought as a lamb to the slaughter, and as a sheep before her shearers is dumb, so he openeth not his mouth.

Matthew 24:15  -  When ye therefore shall see the abomination of desolation, spoken of by Daniel the prophet, stand in the holy place, (whoso readeth, let him understand:)

Mark 13:14  -  But when ye shall see the abomination of desolation, spoken of by Daniel the prophet, standing where it ought not, (let him that readeth understand,) then let them that be in Judaea flee to the mountains:

Isaiah 11:6  -  The wolf also shall dwell with the lamb, and the leopard shall lie down with the kid; and the calf and the young lion and the fatling together; and a little child shall lead them.

2 Timothy 3:16  -  All scripture is given by inspiration of God, and is profitable for doctrine, for reproof, for correction, for instruction in righteousness:

2 Peter 1:21  -  For the prophecy came not in old time by the will of man: but holy men of God spake as they were moved by the Holy Ghost.

Hebrews 4:12  -  For the word of God is quick, and powerful, and sharper than any twoedged sword, piercing even to the dividing asunder of soul and spirit, and of the joints and marrow, and is a discerner of the thoughts and intents of the heart.

Revelation 22:19  -  And if any man shall take away from the words of the book of this prophecy, God shall take away his part out of the book of life, and out of the holy city, and from the things which are written in this book.

Psalms 119:9  -  Wherewithal shall a young man cleanse his way? by taking heed thereto according to thy word.

Psalms 1:2  -  But his delight is in the law of the LORD; and in his law doth he meditate day and night.

Romans 10:17  -  So then faith cometh by hearing, and hearing by the word of God.

1 John 5:7  -  For there are three that bear record in heaven, the Father, the Word, and the Holy Ghost: and these three are one.

Ephesians 1:13  -  In whom ye also trusted, after that ye heard the word of truth, the gospel of your salvation: in whom also after that ye believed, ye were sealed with that holy Spirit of promise,

2 Corinthians 5:21  -  For he hath made him to be sin for us, who knew no sin; that we might be made the righteousness of God in him.

1 Peter 2:22  -  Who did no sin, neither was guile found in his mouth:

1 John 3:5  -  And ye know that he was manifested to take away our sins; and in him is no sin.

Hebrews 4:15  -  For we have not an high priest which cannot be touched with the feeling of our infirmities; but was in all points tempted like as we are, yet without sin.

Hebrews 7:26  -  For such an high priest became us, who is holy, harmless, undefiled, separate from sinners, and made higher than the heavens;

1 Peter 1:2  -  Elect according to the foreknowledge of God the Father, through sanctification of the Spirit, unto obedience and sprinkling of the blood of Jesus Christ: Grace unto you, and peace, be multiplied.

Revelation 20:10  -  And the devil that deceived them was cast into the lake of fire and brimstone, where the beast and the false prophet are, and shall be tormented day and night for ever and ever. 

Revelation 20:11  -  And I saw a great white throne, and him that sat on it, from whose face the earth and the heaven fled away; and there was found no place for them. 

Revelation 20:12  -  And I saw the dead, small and great, stand before God; and the books were opened: and another book was opened, which is the book of life: and the dead were judged out of those things which were written in the books, according to their works. 

Revelation 20:13  -  And the sea gave up the dead which were in it; and death and hell delivered up the dead which were in them: and they were judged every man according to their works. 

Revelation 20:14  -  And death and hell were cast into the lake of fire. This is the second death. 

Revelation 20:15  -  And whosoever was not found written in the book of life was cast into the lake of fire.

2 Timothy 2:12  -  If we suffer, we shall also reign with him: if we deny him, he also will deny us:

John 8:44  -  Ye are of your father the devil, and the lusts of your father ye will do. He was a murderer from the beginning, and abode not in the truth, because there is no truth in him. When he speaketh a lie, he speaketh of his own: for he is a liar, and the father of it.

1 Thessalonians 5:23  -  And the very God of peace sanctify you wholly; and I pray God your whole spirit and soul and body be preserved blameless unto the coming of our Lord Jesus Christ.

1 Timothy 3:15  -  But if I tarry long, that thou mayest know how thou oughtest to behave thyself in the house of God, which is the church of the living God, the pillar and ground of the truth.

1 Timothy 2:12  -  But I suffer not a woman to teach, nor to usurp authority over the man, but to be in silence.

1 Peter 5:8  -  Be sober, be vigilant; because your adversary the devil, as a roaring lion, walketh about, seeking whom he may devour:

Ephesians 4:22  -  That ye put off concerning the former conversation the old man, which is corrupt according to the deceitful lusts;

Colossians 3:9  -  Lie not one to another, seeing that ye have put off the old man with his deeds;

Hebrews 1:14  -  Are they not all ministering spirits, sent forth to minister for them who shall be heirs of salvation?

2 Corinthians 11:14  -  And no marvel; for Satan himself is transformed into an angel of light.

2 Corinthians 5:8  -  We are confident, I say, and willing rather to be absent from the body, and to be present with the Lord.

2 Peter 1:14  -  Knowing that shortly I must put off this my tabernacle, even as our Lord Jesus Christ hath shewed me.

1 Thessalonians 4:14  -  For if we believe that Jesus died and rose again, even so them also which sleep in Jesus will God bring with him.

James 2:26  -  For as the body without the spirit is dead, so faith without works is dead also.

Exodus 31:13  -  Speak thou also unto the children of Israel, saying, Verily my sabbaths ye shall keep: for it is a sign between me and you throughout your generations; that ye may know that I am the LORD that doth sanctify you. 

Matthew 1:23  -  Behold, a virgin shall be with child, and shall bring forth a son, and they shall call his name Emmanuel, which being interpreted is, God with us.

Acts 20:28  -  Take heed therefore unto yourselves, and to all the flock, over the which the Holy Ghost hath made you overseers, to feed the church of God, which he hath purchased with his own blood.

John 1:1  -  In the beginning was the Word, and the Word was with God, and the Word was God.

Philippians 2:6  -  Who, being in the form of God, thought it not robbery to be equal with God:

Colossians 1:15  -  Who is the image of the invisible God, the firstborn of every creature:

Colossians 2:9  -  For in him dwelleth all the fulness of the Godhead bodily.

1 Timothy 1:15  -  This is a faithful saying, and worthy of all acceptation, that Christ Jesus came into the world to save sinners; of whom I am chief. 

1 Timothy 1:16  -  Howbeit for this cause I obtained mercy, that in me first Jesus Christ might shew forth all longsuffering, for a pattern to them which should hereafter believe on him to life everlasting. 

1 Timothy 1:17  -  Now unto the King eternal, immortal, invisible, the only wise God, be honour and glory for ever and ever. Amen. 

Hebrews 1:3  -  Who being the brightness of his glory, and the express image of his person, and upholding all things by the word of his power, when he had by himself purged our sins, sat down on the right hand of the Majesty on high:

2 Timothy 2:16  -  But shun profane and vain babblings: for they will increase unto more ungodliness.

Hebrews 1:3  -  Who being the brightness of his glory, and the express image of his person, and upholding all things by the word of his power, when he had by himself purged our sins, sat down on the right hand of the Majesty on high:

Titus 2:13  -  Looking for that blessed hope, and the glorious appearing of the great God and our Saviour Jesus Christ;

1 John 3:16  -  Hereby perceive we the love of God, because he laid down his life for us: and we ought to lay down our lives for the brethren.

Revelation 14:10  -  The same shall drink of the wine of the wrath of God, which is poured out without mixture into the cup of his indignation; and he shall be tormented with fire and brimstone in the presence of the holy angels, and in the presence of the Lamb:

Revelation 20:10  -  And the devil that deceived them was cast into the lake of fire and brimstone, where the beast and the false prophet are, and shall be tormented day and night for ever and ever.

Matthew 25:46  -  And these shall go away into everlasting punishment: but the righteous into life eternal.

Romans 16:17  -  Now I beseech you, brethren, mark them which cause divisions and offences contrary to the doctrine which ye have learned; and avoid them.

Romans 15:6  -  That ye may with one mind and one mouth glorify God, even the Father of our Lord Jesus Christ.

1 Corinthians 1:10  -  Now I beseech you, brethren, by the name of our Lord Jesus Christ, that ye all speak the same thing, and that there be no divisions among you; but that ye be perfectly joined together in the same mind and in the same judgment.
