#!/usr/bin/env python3
import shelve, sys
from os.path import dirname, realpath
from string import punctuation as punct

usage = "\n\n"+' '*15+"The Holy Bible - King James Version\n\n\n"+' '*12+"Us"+\
"age:   bible [ [-s|-f|-c] <query> | <reference>]\n\n"+' '*12+"-s    Search "+\
"for all scriptures with those words.\n"+' '*12+"-f    Find the first mentio"+\
"n of a given query.\n"+' '*12+"-c    Count the occurances of a given query."+\
"\n\n"+' '*12+"--stats   Count of chapters, verses, and words in each book,"+\
"\n"+' '*24+"and in the whole bible.  Counted on the fly.\n\n"+' '*12+"Examp"+\
"les:\n"""+' '*16+"bible 1 John 3:16\n"+' '*16+"bible jn 3 16\n"+' '*16+"bib"+\
"le sos 1:1\n"+' '*16+"bible gen 8:4-6\n"+' '*16+"bible -c grace\n"+' '*16+\
"bible -f redeemed\n"+' '*16+"bible -s what manner of love\n"+' '*16+"bible "+\
"-s none other name\n"+' '*16+"bible --stats\n\n"

if len(sys.argv) == 1:
	print(usage)
	exit()
elif sys.argv[1] in ('-f','-s','-c'):
	q = ' '.join(sys.argv[2:])
	book = ''
	with open(dirname(realpath(__file__)) + '/kjv.min') as f:
		if sys.argv[1] == '-c':
			ot, nt = f.read().split('Book 40	Matthew')
			ot = ot.count(q)
			nt = nt.count(q)
			c = ot + nt
			print("\n\tThe word '%s' occurs %s times." % (q, c))
			print("\t\tOT - %s | NT - %s\n" % (ot, nt))
		else:
			for l in f:
				if l.startswith('Book '): b = ' '.join(l.split()[2:])
				elif q.lower() in l.lower():
					c, v = l.split()[0].split(':')
					l = ' '.join(l.split()[1:])
					c = c.lstrip('0')
					v = v.lstrip('0')
					print("\n%s %s:%s  -  %s\n" % (b, c, v, l))
					if sys.argv[1] == '-f': break

	exit()

bible = shelve.open('/opt/pykjb-tools/kjv.shelvedb')
if sys.argv[1] == '--stats':
	import string
	bible_ch = 0
	uniq_words = []
	for book in bible:
		bible_v = bible_w = book_v = book_w = 0
		bible_ch += len(bible[book])
		for ch in bible[book]:
			ch_v = len(bible[book][ch])
			book_v += ch_v
			bible_v += ch_v
			ch_w = 0
			for v in bible[book][ch]:
				ch_w += len(bible[book][ch][v].split(' '))			
				t = (''.join(c for c in bible[book][ch][v] if c not in punct))
				for w in t.split(' '):
					if not w in uniq_words: uniq_words.append(w)

			book_w += ch_w
			bible_w += ch_w
			
		print("%s\n\t  Chapters: %s\n\t  Verses: %s\n\t  Words: %s" %
		        (book, len(bible[book]), book_v, book_w))
			
	print("Books: %s\nChapters: %s\nVerses: %s\nWords: %s\nUnique words: %s" %
	        (len(bible), bible_ch, bible_v, bible_w, len(uniq_words)))
	exit()

q = ' '.join(sys.argv[1:]).replace(":", " ").split()
if (len(q) == 4) or q[0].isdigit(): q = [' '.join(q[:2])] + q[2:]
if len(q) == 2: q += '~'

try: i, c, v = q
except:
	print(usage + "\n\n Invalid/incomplete input.\n\n")
	exit()

i = i.strip('.').lower()
for a, r in [('jn', 'john'), ('mk', 'mark'), ('lk', 'luke'), ('mt', 'matt'),
    ('js', 'james'), ('dt', 'deut'), ('sos', 'song'), ('rv', 'rev')]:
	if a in i: i = i.replace(a, r)

try: b = [book for book in bible if book.lower().startswith(i)][0]
except:
	print("\n The book '%s' is not recognized.\n" % i)
	exit()

if ('-' in v) or (v == '~'):
	if '-' in v:
		s, f = v.split('-')
		if f < s: f, s = v.split('-')
		s, f = int(s), int(f)
	else: s, f = 1, 99

	print("\n%s %s" % (b, c))
	while not s > f:
		try:
			print("%s  %s " % (str(s), bible[b][c][str(s)]))
			s += 1
		except:
			try: print("\n - %s %s has %s verses -" % (b, c, len(bible[b][c])))
			except: print("\n - %s has %s chapters -" % (b, len(bible[b])))
			break

	print("\n")
else:
	try: print("\n%s %s:%s  -  %s\n" % (b, c, v, bible[b][c][v]))
	except:
		try: print("\n - %s %s has %s verses -\n" % (b, c, len(bible[b][c])))
		except: print("\n - %s has %s chapters -\n" % (b, len(bible[b])))

bible.close()
