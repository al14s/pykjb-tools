The Coder's Bible Toolset

  by Adam Byers (al14s)


Files:
    kjv.txt         from https://web.archive.org/web/20130530223318/http://patriot.net/~bmcgin/kjv12.txt

    kjv.min         text format optimized for Bible-wide searches (word/phrase)

    kjv.shelvedb    the Bible in a Shelve database

    parse_bible.py  used to generate the shelvedb file from the .txt file.

    bible.py        commandline query tool for the Bible (references and word
                        searches)

    verse_kata.py   verse memorization tool with levels and three methods

    mem_verses.txt  list of verses for memorization

    bot.py          irc bot that displays verses upon command.
                        example:   !v rom 5 8
