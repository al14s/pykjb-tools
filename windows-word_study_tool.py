#!/usr/bin/env python3
import os
import PySimpleGUI as sg

bible_file = 'kjv.min'
tmp_file = '.tmp.txt'

layout = [[sg.Output(size=(110, 30), key='-OUTPUT-', font=('Helvetica 10'))],
          [sg.Input(key='-QUERY-', do_not_clear=False),
          sg.Button('Search', bind_return_key=True),
          sg.Button('Save', bind_return_key=True),
          sg.Button('Print'),]]
window = sg.Window("Bible Word Study Tool", layout, font=('Helvetica', ' 13'))
try: os.remove(tmp_file)
except: pass

while True:     # The Event Loop
    event, value = window.read()
    if event in (None, 'EXIT'):
        try: os.remove(tmp_file)
        except: pass
        break

    if event in ('Save'): os.startfile(tmp_file, "open")
    if event in ('Print'): os.startfile(tmp_file, "print")        
    if event == 'Search':
        q = value['-QUERY-'].rstrip()
        if not q: continue
        d = open(bible_file).read()
        ot, nt = d.split('Book 40	Matthew')
        ot = ot.count(q)
        nt = nt.count(q)
        c = ot + nt
        try: os.remove(tmp_file)
        except: pass

        window.FindElement('-OUTPUT-').Update('')

        with open(tmp_file, 'a') as wf:
            o = "\nThe word '%s' occurs %s time(s)." % (q, c)
            o += "\n\tOT - %s | NT - %s\n%s" % (ot, nt, "-" * 52)
            print(o)
            wf.write(o)

            with open(bible_file) as f:           
                for l in f:
                    if l.startswith('Book '): b = ' '.join(l.split()[2:])
                    elif q.lower() in l.lower():
                        c, v = l.split()[0].split(':')
                        l = ' '.join(l.split()[1:])
                        c = c.lstrip('0')
                        v = v.lstrip('0')
                        o = "\n\n%s %s:%s  -  %s" % (b, c, v, l)
                        print(o)
                        wf.write(o)
