#!/usr/bin/env python2
from twisted.words.protocols import irc
from twisted.internet import reactor, protocol
from twisted.python import log
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.internet.defer import Deferred
from twisted.internet.protocol import ClientFactory
from twisted.words.protocols.irc import IRCClient
from twisted.protocols.policies import SpewingFactory
from txsocksx.client import SOCKS5ClientEndpoint
from txsocksx.tls import TLSWrapClientEndpoint
import time, sys, shelve

irc_server = "irc.freenode.net"
irc_server_port = 6667
channel = "#kjv-bot-test"
log_file = "bot_log.txt"
use_proxy = False
socks5_server = ""
socks5_server_port = None
socks5_username = ""
socks5_password = ""

bible = shelve.open('kjv.shelvedb')
refs = {}
for book in bible:
    refs[book.lower()] = book
    if " " in book:    
        refs[book[:4].lower()] = book            
        if len(book.split()[1]) > 4: refs[book[:5].lower()] = book
        if len(book.split()[1]) > 5: refs[book[:6].lower()] = book
        if len(book.split()[1]) > 6: refs[book[:7].lower()] = book
        if "John" in book:
            x = book.split()[0]
            refs[x + " jn"] = x + " John"
    
    else:
        refs[book[:3].lower()] = book
        if len(book) > 3: refs[book[:4].lower()] = book
        if len(book) > 4: refs[book[:5].lower()] = book
        if book == "John":refs["jn"] = "John"

class MessageLogger:
    def __init__(self, file): self.file = file

    def log(self, message):
        timestamp = time.strftime("[%H:%M:%S]", time.localtime(time.time()))
        self.file.write('%s %s\n' % (timestamp, message))
        self.file.flush()

    def close(self): self.file.close()

class LogBot(irc.IRCClient): 
    nickname = "kjv-bible-bot"
    
    def connectionMade(self):
        irc.IRCClient.connectionMade(self)
        self.logger = MessageLogger(open(self.factory.filename, "a"))
        self.logger.log("[connected at %s]" % 
                        time.asctime(time.localtime(time.time())))

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)
        self.logger.log("[disconnected at %s]" % 
                        time.asctime(time.localtime(time.time())))
        self.logger.close()

    def signedOn(self): self.join(self.factory.channel)

    def joined(self, channel): self.logger.log("[I have joined %s]" % channel)

    def privmsg(self, user, channel, msg):
        user = user.split('!', 1)[0]
        if msg.startswith('!v'):
            try:
                c1 = msg.replace("!v", '').replace(".", " ").strip().split()
                if len(c1) == 4: c1 = [' '.join(c1[:2])] + c1[2:]
                b, c, v = c1
                b = b.strip('.').lower()
                msg = "%s %s:%s  -  %s" % (refs[b], c, v, bible[refs[b]][c][v])
            except:
                msg = "Invalid input -\n Try  <book> <chapter> <verse>  " + \
                    "or   <book> <chapter>.<verse>"
    
            self.msg(channel, msg)
        
        self.logger.log("<%s> %s" % (user, msg))

    def action(self, user, channel, msg):
        user = user.split('!', 1)[0]
        self.logger.log("* %s %s" % (user, msg))
        
    def irc_NICK(self, prefix, params):
        old_nick = prefix.split('!')[0]
        new_nick = params[0]
        self.logger.log("%s is now known as %s" % (old_nick, new_nick))
        
    def alterCollidedNick(self, nickname): return nickname + '^'

class LogBotFactory(protocol.ClientFactory):
    def __init__(self, channel, filename):
        self.channel = channel
        self.filename = filename

    def buildProtocol(self, addr):
        p = LogBot()
        p.factory = self
        return p

    def clientConnectionLost(self, connector, reason): connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print("connection failed:" + reason)
        reactor.stop()

if __name__ == '__main__':
    log.startLogging(sys.stdout)
    f = LogBotFactory(channel, log_file)    
    if use_proxy:
        ProxyEndpoint = TCP4ClientEndpoint(
            reactor, socks5_server, socks5_server_port)
        ServerEndpoint = SOCKS5ClientEndpoint(
            irc_server, irc_server_port, ProxyEndpoint,
            methods={'login': (socks5_username, socks5_password)})
        deferred = ServerEndpoint.connect(f)                 
    else: reactor.connectTCP(irc_server, irc_server_port, f)
                              
    reactor.run()
