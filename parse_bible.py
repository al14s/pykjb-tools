#!/usr/bin/env python3
import shelve, sys, os

dat = open('kjv.txt').read()  # our source
bible = shelve.open('kjv.shelvedb', writeback=True)  # first output
books = dat.split("Book")
for book in books:
    curr_chapter = curr_verse = 0
    if not book.strip(): continue
    lines = book.split('\n')
    title = ' '.join(lines[0].split()[1:])
    verse = []
    for line in lines[1:]:
        if not line.strip(): continue
        if (line.startswith('0') or line.startswith('1')):
            if int(curr_chapter) > 0:
                try: bible[title][curr_chapter][curr_verse] = ' '.join(verse)
                except:
                    new = {curr_verse: ' '.join(verse)}
                    try: bible[title][curr_chapter] = new
                    except: bible[title] = {curr_chapter: new} 
                           
            curr_chapter = line.split(':')[0].lstrip('0')
            curr_verse = line.split()[0].split(':')[1].lstrip('0')
            verse = line.split(' ')[1:]  
        else: verse += line.lstrip("        ").split(' ')
        
    bible[title][curr_chapter][curr_verse] = ' '.join(verse)

bible.close()
os.system("cat kjv.txt | tr '\n' '\f' | sed 's/\f      "+\
            "  / /g' | tr '\f' '\n' | sed -r '/^\s*$/d' > kjv.min") # 2nd out
