#!/usr/bin/env python3
from sys import argv
from os.path import dirname, realpath
from random import randint

print("\n"+" "*13+"/\      Scripture Kata\n /"+"v"*12+" \\"+"-"*40+ \
      ",\n `"+"^"*12+" /"+"="*39+'"\n'+" "*13+"\/"+" "*21+"Psalm 119:11\n" + \
        "\n\t[-l|-f|-fl] <level> <size of verse group>\n")
n, g, sw = 5, False, "-l"  # defaults and arg parsing
try:
    sw = argv[1]
    n = int(argv[2])
    g = int(argv[3])
except:
    if len(argv) < 2: exit()

vl = open(dirname(realpath(__file__)) + "/mem_verses.txt").readlines()
while "\n" in vl: vl.pop(vl.index("\n"))  # file read + prep

if g:  # select a random group of N verses
    tl = vl
    vl = []
    while len(vl) < g: vl = list(set(vl + [tl[randint(0,len(tl)-1)]]))

for v in vl:  # main loop
    vs = v.split(' - ')
    if sw in ("-l","-fl"):  # substitute N random words for blanks
        vt = vs[1].strip().split(' ')
        if len(vt) < n: n = len(vt)
        while vt.count("___") < n: vt[randint(0,len(vt)-1)] = "___"
        vs[1] = ' '.join(vt)

    if sw == "-l": print(vs[0] + "  -  " + vs[1])  # display verse
    elif sw in ("-f","-fl"):  # reveal either addr or text and wait for user
        r = randint(0,1)
        print(vs[r])
        input()  # after user 'enter', show the corresponding text or addr
        print(vs[abs(r-1)] + "\n\n-------------------------------")

    input()  # after user 'enter', on to the next verse
    